#include "LinkedList.h"
#include <stdio.h>

int main (int argc, char** argv) {
  struct LinkedList* list = create("Hello");
  insertAfter(list, "Hello", " ");
  insertAfter(list, " ", "World");
  insertAfter(list, "World", "\n");
  
  struct Node* tmp = list->head;
  while (tmp != NULL) {
    printf ("%s", tmp->data);
    tmp = tmp->next;
  }
}