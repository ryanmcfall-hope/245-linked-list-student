#include <stdlib.h>
#include <string.h>
#include "LinkedList.h"

struct Node* createNode (char* data) {
  struct Node* node = (struct Node*) malloc(sizeof(struct Node));
  node->next = NULL;
  node->data = data;
  return node;
}

struct LinkedList* create (char* data) {
  struct LinkedList* newList = (struct LinkedList*) malloc(sizeof(struct LinkedList));
  newList->head = createNode(data);
  return newList;
}

struct LinkedList* insertAfter (struct LinkedList* list, char* insertNodeValue, char* newValue) {
  struct Node* currentNode = list->head;
  while (currentNode != NULL && strcmp(currentNode->data, insertNodeValue) != 0) {
    currentNode = currentNode->next; 
  }
  
  //  If we didn't find the value, return NULL to indicate that fact
  if (currentNode == NULL) {
    return NULL;
  }
  
  //  Otherwise, create a new node, and set up the pointers appropriately
  struct Node* newNode = createNode(newValue);
  newNode->next = currentNode->next;
  currentNode->next = newNode;
  return list;
}
