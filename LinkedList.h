struct Node {
  struct Node* next;
  char* data;
};

struct LinkedList {
  struct Node* head;
};

struct LinkedList* create(char* data);
struct LinkedList* insertAfter(struct LinkedList* list, char* valueToFind, char* newValue);
